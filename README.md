# Suckless Terminal
My personal build of the suckless terminal (st)

## Applied patches

| name                       | description                                                 |
| -------------------------- | ----------------------------------------------------------- |
| anysize                    | Evenly spreads the extra pixels around the border           |
| clipboard                  | Fixes the clipboard using both cipboards                    |
| scrollback                 | Adds scrollback                                             |
| scrollback-mouse           | Adds scrollback with shift-mouse                            |
| scrollback-mouse-altscreen | Enables smart scrollback when not in alt-screen (less, etc) |
| vertcenter                 | aligns the text in the center of lines for some fonts       |
| xclearwin                  | Fix the black borders with pywal
| xresources                 | pywal and xrdb customization                                |

## Customize with Xresources
Add this to your `.Xresources` file and make sure it is loaded by `xrdb ~/.Xresources`
```
*.font: SauceCodePro Nerd Font Mono:size=14:antialias=true:autohint=true;
```
